package com.cpt202.pizza.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cpt202.pizza.models.ShoppingCartProduct;

public interface ShoppingCartRepo extends JpaRepository<ShoppingCartProduct, Integer>{
    
    
}
