package com.cpt202.pizza.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class ShoppingCartProduct {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String pizzaName;
    private int pizzaSize;
    private String pizzaBase;
    private int num;
    private double unitPrice;

    public ShoppingCartProduct() {
    }
    
    public ShoppingCartProduct(int id, String pizzaName, int pizzaSize, String pizzaBase, int num, double unitPrice) {
        this.id = id;
        this.pizzaName = pizzaName;
        this.pizzaSize = pizzaSize;
        this.pizzaBase = pizzaBase;
        this.num = num;
        this.unitPrice = unitPrice;
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getPizzaName() {
        return pizzaName;
    }
    public void setPizzaName(String pizzaName) {
        this.pizzaName = pizzaName;
    }
    public int getPizzaSize() {
        return pizzaSize;
    }
    public void setPizzaSize(int pizzaSize) {
        this.pizzaSize = pizzaSize;
    }
    public String getPizzaBase() {
        return pizzaBase;
    }
    public void setPizzaBase(String pizzaBase) {
        this.pizzaBase = pizzaBase;
    }
    public int getNum() {
        return num;
    }
    public void setNum(int num) {
        this.num = num;
    }
    public double getUnitPrice() {
        return unitPrice;
    }
    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }
    
}
