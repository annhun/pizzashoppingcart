package com.cpt202.pizza.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.cpt202.pizza.models.ShoppingCartProduct;
import com.cpt202.pizza.repositories.ShoppingCartRepo;




@Service
public class ShoppingCartService {

    @Autowired
    ShoppingCartRepo shoppingCartRepo; 

    public List<ShoppingCartProduct> getShoppingCartList(){
        return shoppingCartRepo.findAll(Sort.by("id"));
    }
    
    public int getTotalNum(){
        int totalNum = 0;
        List<ShoppingCartProduct> shoppingCartList = shoppingCartRepo.findAll();
        for(ShoppingCartProduct product: shoppingCartList){
            totalNum += product.getNum();
        }
         return totalNum;
    }
        
    public double getTotalAmount(){
            double totalAmount = 0.0;
            List<ShoppingCartProduct> shoppingCartList= shoppingCartRepo.findAll();
            for(ShoppingCartProduct product: shoppingCartList){
                totalAmount += product.getNum() * product.getUnitPrice(); 
            }
            String str = String.format("%.2f", totalAmount);
            totalAmount = Double.parseDouble(str);
            return totalAmount;
    }

    public void editNum(int id, int num){
        Optional<ShoppingCartProduct> returnedProduct = shoppingCartRepo.findById(id);
        if(returnedProduct.isPresent()){
            ShoppingCartProduct product = returnedProduct.get();
            if(num >= 1000){
                product.setNum(1000);
                shoppingCartRepo.save(product);
            }
            else if(num > 0){
                product.setNum(num);
                shoppingCartRepo.save(product);
            }
            else{
                shoppingCartRepo.deleteById(id);
            } 
        }
        else{
        } 
    }

    public void empty(){
        shoppingCartRepo.deleteAll();
    }

    public boolean isEmpty(){
        List<ShoppingCartProduct> shoppingCartList= shoppingCartRepo.findAll();
        return shoppingCartList.isEmpty();
    }

}