package com.cpt202.pizza.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.cpt202.pizza.services.ShoppingCartService;


@Controller
@RequestMapping("/pizza")
public class ShoppingCartController {
    
    @Autowired
    private ShoppingCartService shoppingCartService;

    
    //localhost:8080/pizza/shoppingcart
    @GetMapping("/shoppingcart")
    public String showShoppingCart(Model model) {
        model.addAttribute("shoppingcartlist", shoppingCartService.getShoppingCartList());
        model.addAttribute("totalnum", shoppingCartService.getTotalNum());
        model.addAttribute("totalamount", shoppingCartService.getTotalAmount());
        if(shoppingCartService.isEmpty()){
            model.addAttribute("emptycartmessage", "No pizza added yet!");
        }
        return "showShoppingCart";
    }

    //localhost:8080/pizza/shoppingcart
    @PostMapping("/shoppingcart")
    public String shoppingCartOperations(@RequestParam(name = "action") String action, 
                                         @RequestParam(name ="id", required = false, defaultValue = "0") int productId, 
                                         @RequestParam(name = "num", required=false, defaultValue = "0") int productNum){
       
            if(action.equals("emptycart")){
                shoppingCartService.empty();
            }
            else{
                shoppingCartService.editNum(productId, productNum);
            }
               
            return "redirect:/pizza/shoppingcart";
           
        
        
        
    }

    
}
